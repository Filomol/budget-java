package hello;

import hello.Service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    private List<Account> accounts = new ArrayList<Account>();

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping("/accounts")
    private List<Account> getAccounts() {
        this.accounts = accountService.findAll();
        return accounts;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/accounts")
    private void createAccount(@RequestBody Account account) {
        accountService.saveAccount(account);
    }
}
