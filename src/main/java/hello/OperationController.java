package hello;

import hello.Service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OperationController {
    @Autowired
    private OperationService operationService;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/operations")
    private List<Operation> getOperations() {
        return operationService.findAll();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/add-operation")
    private void createOperation(@RequestBody Operation operation) {
        operationService.saveOperation(operation);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/edit-operation/{id}")
    private void editOperation(@RequestBody Operation operation) {
        operationService.saveOperation(operation);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/operations", method = RequestMethod.DELETE)
    private void deleteOperation(@RequestParam int deletingId) {
        operationService.deleteById(deletingId);
    }
}
