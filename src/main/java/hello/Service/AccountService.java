package hello.Service;

import hello.Account;
import hello.Repository.AccountsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AccountService {
    @Autowired
    private final AccountsRepository accountsRepository;

    public AccountService(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Transactional
    public void saveAccount(Account account) {
        accountsRepository.save(account);
    }

    @Transactional
    public List<Account> findAll() {
        return accountsRepository.findAll();
    }
}
