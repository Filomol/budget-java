package hello.Service;

import hello.Operation;
import hello.Repository.OperationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class OperationService {

    private OperationsRepository operationsRepository;

    @Autowired
    public OperationService(OperationsRepository operationsRepository) {
        this.operationsRepository = operationsRepository;
    }

    @Transactional
    public void saveOperation(Operation operation) {
        operationsRepository.save(operation);
    }

    @Transactional
    public List<Operation> findAll() {
        return operationsRepository.findAll();
    }

    @Transactional
    public void deleteById(long id) {
        operationsRepository.deleteById(id);
    }
}
