package hello;

import java.util.List;

public class Utils {
    public static Long max(List<Long> numbers) {
        Long max = numbers.get(0);
        for (Long num : numbers) {
            if (max < num) {
                max = num;
            }
        }
        return max;
    }
}
